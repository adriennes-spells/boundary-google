variable "instance_count" {
  type    = number
  default = 3
}

variable "project" {
  type = string
}

variable "zone" {
  type = string
}

variable "network" {
  type = string
}

variable "subnetwork" {
  type = string
}

variable "region" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "key_ring_name" {
  type    = string
  default = ""
}

variable "key_ring_location" {
  type    = string
  default = ""
}

variable "certificate_authority_remote_state_bucket" {
  type = string
}

variable "certificate_authority_remote_state_prefix" {
  type = string
}

variable "dns_managed_zone" {
  type = string
}
